import { Component } from "@angular/core";
import { FormBuilder, FormGroup, Validators, FormArray } from "@angular/forms";
import { map } from 'rxjs/operators';
import { Employee, Address, State, City, countries, states, cities } 
    from "../data-model";
import { of } from "rxjs";
import { ConfirmationService } from "primeng/api";

@Component({
    templateUrl: './reactive-form.component.html'
})
export class ReactiveFormComponent {
    regForm: FormGroup;
    countries: string[] = [];
    states: State[] = [];
    cities: City[] = [];

    minDate = new Date(2018, 4, 12);
    maxDate = new Date();

    selectedCountry: string;
    selectedState: string;

    employees: Employee[] = [];
    
    constructor(private formBuilder: FormBuilder,
        private confirmService: ConfirmationService) { 
        this.createForm();
    }
    
    createForm() {
        this.regForm = this.formBuilder.group({
            firstName: ['', [Validators.required, Validators.minLength(10)]],
            lastName: ['', Validators.required],
            age: ['', Validators.required],
            email: ['', Validators.email], //email
            dateOfBirth: '', //date
            sex: '', //radio            
            physicallyHandicapped: '', //checkbox
            address: this.formBuilder.array([])
            // address: this.formBuilder.group({
            //     country: ['', Validators.required],
            //     state: ['', Validators.required],
            //     city: ['', Validators.required]
            // }) //nested object
        });
    }

    submitForm() {
        const employee = this.prepareSaveReg();        
        this.employees.push(employee);
        this.regForm.reset({
            address: this.formBuilder.array([])
        });
        //console.log(employee);
    }

    prepareSaveReg(): Employee {
        const formModel = this.regForm.value;

        let emp = <Employee>formModel;

        console.log(emp);
        // const addressDeepCopy: Address = formModel.address.map(
        //     (address: Address) => Object.assign({}, address)
        // );

        // const address: Address = {
        //     country: formModel.address.country,
        //     state: formModel.address.state,
        //     city: formModel.address.city
        // };

        // const saveReg: Employee = {
        //     firstName: formModel.firstName as string,
        //     lastName: formModel.lastName as string,
        //     age: formModel.age as number,
        //     email: formModel.email as string,
        //     dateOfBirth: formModel.dateOfBirth as Date,
        //     sex: formModel.sex as string,
        //     physicallyHandicapped: formModel.physicallyHandicapped as boolean,
        //     address: address,
        // };

        return emp;
    }

    onCountryChange(value) {
        this.states = [];
        this.cities = [];
        this.selectedCountry = value;
        let matchingStates = states.filter(item => item.country === value);
        this.states = matchingStates;
    }

    onStateChange(value) {
        this.cities = [];
        this.selectedState = value;
        let matchingCities = cities.filter(item => item.state === value 
            && item.country === this.selectedCountry);
        this.cities = matchingCities;
    }

    get addresses(): FormArray {
        return this.regForm.get('address') as FormArray;
    }

    addAddress() {
        this.confirmService.confirm({
            message: 'Are you sure you want to add an address?',
            accept: () => {
                this.addresses.push(this.formBuilder.group(new Address()));
            }            
        });        
    }

    searchCountry(event) {
        this.countries = countries.filter(ctry => ctry.toUpperCase().indexOf(event.query.toUpperCase()) >= 0);
    }


}