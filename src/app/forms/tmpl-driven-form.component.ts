import { Component } from "@angular/core";
import { Employee } from "../data-model";
import { NgForm } from "@angular/forms";

@Component({
    templateUrl: './tmpl-driven-form.component.html'
})
export class TmplDrivenFormComponent {
    emp: Employee = new Employee();
    employees: Employee[] = [];
    submitted: boolean = false;

    submitForm(form: NgForm) {
        this.employees.push(this.emp);
        this.submitted = true;
        this.emp = new Employee();
        form.reset();
    }
}