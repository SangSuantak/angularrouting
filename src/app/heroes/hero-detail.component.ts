import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { Hero, HeroService } from './hero.service';
import { ActivatedRoute, Router } from '@angular/router';
import { slideInDownAnimation } from '../animations';

@Component({
    selector: 'app-hero-detail',
    templateUrl: './hero-detail.component.html',
    animations: [ slideInDownAnimation ]
})
export class HeroDetailComponent implements OnInit {

    @HostBinding('@routeAnimation')
    routeAnimation = true;

    @HostBinding('style.display')
    display = 'block';

    @HostBinding('style.position')
    position = 'absolute';

    hero: Hero;

    constructor(private route: ActivatedRoute, private router: Router,
        private heroService: HeroService) { }

    ngOnInit() {        
        this.route.paramMap.subscribe(
            params => this.getHero(+params.get('id'))
        );
    }

    getHero(id: number) {
        this.heroService.getHero(id)
            .subscribe(hero => this.hero = hero);
    }

    gotoHeroes(hero: Hero) {
        const heroId = hero ? hero.id : null;
        this.router.navigate(['/heroes', { id: heroId, foo: 'foo' }]);
    }

}


/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/