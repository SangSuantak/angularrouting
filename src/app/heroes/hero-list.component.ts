import { Component, OnInit } from '@angular/core';
import { HeroService, Hero } from './hero.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({  
  templateUrl: './hero-list.component.html',
  styleUrls: ['./hero-list.component.css']
})
export class HeroListComponent implements OnInit {
    heroes: Hero[];
   
    private selectedId: number;
   
    constructor(
      private service: HeroService,
      private route: ActivatedRoute
    ) {}
   
    ngOnInit() {
      this.route.paramMap.pipe(
        switchMap((params: ParamMap) => {
          // (+) before `params.get()` turns the string into a number
          //console.log(params);
          this.selectedId = +params.get('id');
          return this.service.getHeroes();
        })
      ).subscribe(heroes => this.heroes = heroes);
    }
}