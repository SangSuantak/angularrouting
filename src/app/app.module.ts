import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
//import { CrisisCenterComponent } from './crisis-center/crisis-center.component';
//import { HeroListComponent } from './heroes/hero-list.component';
import { PageNotFoundComponent } from './not-found.component';
import { AppRoutingModule } from './app-routing.module';
import { HeroesModule } from './heroes/heroes.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//import { CrisisCenterModule } from './crisis-center/crisis-center.module';
import { ComposeMessageComponent } from './compose-message.component';
//import { AdminModule } from './admin/admin.module';
import { AuthGuard } from './auth-guard.service';
import { AuthService } from './auth.service';
import { LoginComponent } from './login.component';
import { CanDeactivateGuard } from './can-deactivate-guard.service';
import { DialogService } from './dialog.service';
import { ReactiveFormComponent } from "./forms/reactive-form.component";
import { TmplDrivenFormComponent } from './forms/tmpl-driven-form.component';
import { MegaMenuComponent } from './menu/mega-menu.component';
import { MegaMenuModule } from 'primeng/megamenu';
import { CalendarModule } from 'primeng/calendar';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { PipesComponent } from './pipes/pipes.component';
import { ExponentialStrengthPipe } from './pipes/exponential-strength.pipe';
//import { Router, RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    AppComponent,    
    //HeroListComponent,
    PageNotFoundComponent,
    ComposeMessageComponent,
    LoginComponent,
    ReactiveFormComponent,
    TmplDrivenFormComponent,
    MegaMenuComponent,
    PipesComponent,
    ExponentialStrengthPipe
  ],
  imports: [
    BrowserModule,  
    FormsModule,
    ReactiveFormsModule,
    HeroesModule,    
    //CrisisCenterModule,
    //AdminModule,
    AppRoutingModule,    
    BrowserAnimationsModule,
    MegaMenuModule,
    CalendarModule,
    AutoCompleteModule,
    ConfirmDialogModule
  ],
  providers: [DialogService, AuthService, AuthGuard, CanDeactivateGuard, ConfirmationService],
  bootstrap: [AppComponent]
})
export class AppModule { 
  // constructor(router: Router) {
  //   console.log('Routes: ', JSON.stringify(router.config, undefined, 2));
  // }
}
 