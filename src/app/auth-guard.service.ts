import { Injectable } from "@angular/core";
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild, NavigationExtras, CanLoad, Route } from "@angular/router";
import { Observable } from "rxjs";
import { AuthService } from "./auth.service";

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {
    
    canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
        return this.canActivate(childRoute, state);
    }
    constructor(private authService: AuthService,
        private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): 
    boolean | Observable<boolean> | Promise<boolean> {
        let url: string = state.url;
        
        return this.checkLogin(url);
    }

    checkLogin(url: string): boolean {
        if(this.authService.isLoggedIn) {
            return true;
        }

        this.authService.redirectUrl = url;
        
        let navigationExtras: NavigationExtras = {
            queryParams: { 'session_id': 123456789 },
            fragment: 'anchor'
        }

        this.router.navigate(['/login'], navigationExtras);
        return false;
    }

    canLoad(route: Route): boolean | Observable<boolean> | Promise<boolean> {
        console.log(route);
        let url = `/${route.path}`;
        return this.checkLogin(url);
    }
}