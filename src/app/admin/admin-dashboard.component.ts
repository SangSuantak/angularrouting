import { Component, OnInit } from "@angular/core";
import { SelectivePreloadingStrategy } from "../selective-preloading-strategy";

@Component({
    templateUrl: './admin-dashboard.component.html'
})
export class AdminDashboardComponent implements OnInit {
    modules: string[] = [];

    constructor(private preloadStrategy: SelectivePreloadingStrategy) { }
    
    ngOnInit() {
        this.modules = this.preloadStrategy.preloadedModules;
    }
}