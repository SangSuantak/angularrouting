import { Component, OnInit } from "@angular/core";
import { AuthService } from "./auth.service";
import { Router, NavigationExtras } from "@angular/router";

@Component({
    templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
    message: string;

    constructor(public authService: AuthService, private router: Router) { }

    ngOnInit() {
        this.setMessage();
    }

    setMessage() {
        this.message = 'Logged ' +
            (this.authService.isLoggedIn ? 'in' : 'out');
    }

    login() {
        this.message = 'Trying to log in ...';

        this.authService.login().subscribe(
            () => {
                this.setMessage();
                if(this.authService.isLoggedIn) {
                    let redirect = this.authService.redirectUrl ?
                        this.authService.redirectUrl
                        : '/';

                    let navigationExtras: NavigationExtras = {
                        queryParamsHandling: 'merge',
                        preserveFragment: true
                    }

                    this.router.navigate([redirect], navigationExtras);
                }
            }
        );
    }

    logout() {
        this.authService.logout();
        this.setMessage();
    }
}