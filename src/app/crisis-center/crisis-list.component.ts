import { Component, OnInit } from '@angular/core';
import { Crisis, CrisisService } from './crisis.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

@Component({  
  templateUrl: './crisis-list.component.html',
  styleUrls: ['./crisis-list.component.css']
})
export class CrisisListComponent implements OnInit {
    crises: Crisis[];
   
    private selectedId: number;
   
    constructor(
      private service: CrisisService,
      private route: ActivatedRoute
    ) {}
   
    ngOnInit() {
      this.route.paramMap.pipe(
        switchMap((params: ParamMap) => {
          // (+) before `params.get()` turns the string into a number
          //console.log(params);
          this.selectedId = +params.get('id');
          return this.service.getCrises();
        })
      ).subscribe(crises => this.crises = crises);
    }
}