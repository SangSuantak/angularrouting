import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { Crisis, CrisisService } from './crisis.service';
import { ActivatedRoute, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { slideInDownAnimation } from '../animations';
import { CanDeactivateGuard, CanComponentDeactivate } from '../can-deactivate-guard.service';
import { Observable } from 'rxjs';
import { DialogService } from '../dialog.service';

@Component({
    //selector: 'app-crisis-detail',
    templateUrl: './crisis-detail.component.html',
    animations: [ slideInDownAnimation ]
})
export class CrisisDetailComponent implements OnInit {
    
    @HostBinding('@routeAnimation')
    routeAnimation = true;

    @HostBinding('style.display')
    display = 'block';

    @HostBinding('style.position')
    position = 'absolute';

    crisis: Crisis;
    editName: string;    

    constructor(private route: ActivatedRoute, private router: Router,
        private crisisService: CrisisService,
        private dialogService: DialogService) { }

    canDeactivate(component: CanComponentDeactivate, 
        currentRoute: ActivatedRouteSnapshot, 
        currentState: RouterStateSnapshot, nextState?: 
            RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
        if(!this.crisis || this.crisis.name === this.editName) {
            return true;
        }

        return this.dialogService.confirm('Discard changes?');
    }

    ngOnInit() {        
        // this.route.paramMap.subscribe(
        //     params => this.getCrisis(+params.get('id'))
        // );
        this.route.data
            .subscribe(
                (data: { crisis: Crisis }) => {
                    this.crisis = data.crisis;
                    this.editName = this.crisis.name;
                }
            );
    }

    getCrisis(id: number) {
        this.crisisService.getCrisis(id)
            .subscribe(crisis => {
                this.crisis = crisis;
                this.editName = this.crisis.name;
            });
    }

    gotoCrises(crisis: Crisis) {
        const crisisId = crisis ? crisis.id : null;
        this.router.navigate(['/crisis-center', { id: crisisId, foo: 'foo' }]);
    }

}


/*
Copyright 2017-2018 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/