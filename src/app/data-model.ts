export const countries: string[] = [
    "India",
    "US",
    "Sweden",
    "Sri Lanka"
];

export const states: State[] = [
    {
        name: 'Delhi',
        country: 'India'
    },
    {
        name: 'Haryana',
        country: 'India'
    },
    {
        name: 'Stockholm',
        country: 'Sweden'
    },
    {
        name: 'New York',
        country: 'US'
    },
    {
        name: 'Colombo',
        country: 'Sri Lanka'
    },
    {
        name: 'California',
        country: 'US'
    },
    {
        name: 'Utah',
        country: 'US'
    }
];

export const cities: City[] = [
    {
        name: 'New Delhi',
        state: 'Delhi',
        country: 'India'
    },
    {
        name: 'Gurgaon',
        state: 'Haryana',        
        country: 'India'
    },
    {
        name: 'Stockholm',
        state: 'Stockholm',
        country: 'Sweden'
    },
    {
        name: 'Manhattan',
        state: 'New York',
        country: 'US'
    },
    {
        name: 'Colombo',
        state: 'Colombo',
        country: 'Sri Lanka'
    },
    {
        name: 'Sacramento',
        state: 'California',
        country: 'US'
    },
    {
        name: 'Salt Lake City',
        state: 'Utah',
        country: 'US'
    }
];

export class Address {
    city = '';
    state = '';
    country = '';
}

export class Employee {
    firstName: string = '';
    lastName: string = '';
    age: number = null;
    email: string = '';
    dateOfBirth: Date = null;
    sex: string = '';
    physicallyHandicapped: boolean = false;
    address: Address[] = [];    
}

export class State {
    name: string = '';
    country: string = '';
}

export class City {
    name: string = '';
    state: string = '';
    country: string = '';
}