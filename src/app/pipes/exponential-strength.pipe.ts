import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: 'exponentialStrength'
})
export class ExponentialStrengthPipe implements PipeTransform {
    /* transform(value: any, ...args: any[]) {
        throw new Error("Method not implemented.");
    } */

    transform(value: number, exponent: string) {
        let exp = parseFloat(exponent);
        return Math.pow(value, exp);
    }
}