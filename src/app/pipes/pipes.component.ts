import { Component, OnInit } from "@angular/core";
import { Observable, interval } from "rxjs";
import { take, map } from "rxjs/operators";

@Component({
    templateUrl: './pipes.component.html'
})
export class PipesComponent implements OnInit {
    currencyCode: string = 'USD';
    messages: Observable<string>;
    messageList: [
        'Lian', 'Moi', 'Neu', 'Sang'
    ]

    constructor() {
        this.getMessages();
    }

    ngOnInit() {
        this.getMessages();
    }

    getMessages() {
        this.messages = interval(500)
            .pipe(
                map(i => this.messageList[i]),
                take(this.messageList.length)
            );
    }

}