import { NgModule } from "@angular/core";
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { CrisisCenterComponent } from "./crisis-center/crisis-center.component";
//import { HeroListComponent } from "./heroes/hero-list.component";
import { PageNotFoundComponent } from "./not-found.component";
import { ComposeMessageComponent } from "./compose-message.component";
import { LoginComponent } from "./login.component";
import { AuthGuard } from "./auth-guard.service";
import { SelectivePreloadingStrategy } from "./selective-preloading-strategy";
import { ReactiveFormComponent } from "./forms/reactive-form.component";
import { TmplDrivenFormComponent } from "./forms/tmpl-driven-form.component";
import { MegaMenuComponent } from "./menu/mega-menu.component";
import { PipesComponent } from "./pipes/pipes.component";

const routes: Routes = [
    //{ path: 'crisis-center', component: CrisisCenterComponent },
    //{ path: 'heroes', component: HeroListComponent },
    { 
        path: 'admin', 
        loadChildren: 'app/admin/admin.module#AdminModule',
        canLoad: [ AuthGuard ]
    },
    {
        path: 'crisis-center',
        loadChildren: 'app/crisis-center/crisis-center.module#CrisisCenterModule',
        data: {
            preload: true
        }
    },
    {
        path: 'reactive-form',
        component: ReactiveFormComponent
    },
    {
        path: 'tmpl-driven-form',
        component: TmplDrivenFormComponent
    },
    {
        path: 'mega-menu',
        component: MegaMenuComponent
    },
    {
        path: 'pipes',
        component: PipesComponent
    },
    { 
        path: 'compose', 
        component: ComposeMessageComponent, 
        outlet: 'popup' 
    },
    { 
        path: 'login', 
        component: LoginComponent 
    },
    { 
        path: '', 
        redirectTo: '/heroes', 
        pathMatch: 'full' 
    },
    { 
        path: '**', 
        component: PageNotFoundComponent 
    }
  ];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, { 
            enableTracing: false,
            preloadingStrategy: SelectivePreloadingStrategy
        })
    ],
    exports: [ RouterModule ],
    providers: [ SelectivePreloadingStrategy ]
})
export class AppRoutingModule {

}
